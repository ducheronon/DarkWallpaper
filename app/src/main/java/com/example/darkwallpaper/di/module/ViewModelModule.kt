package com.example.darkwallpaper.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.darkwallpaper.di.ViewModelFactory
import com.example.darkwallpaper.di.ViewModelKey
import com.example.darkwallpaper.ui.bottomnav.BottomNavViewModel
import com.example.darkwallpaper.ui.home.HomeViewModel
import com.example.darkwallpaper.ui.photodetail.PhotoDetailViewModel
import com.example.darkwallpaper.ui.splash.SpLashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/** Created by Duc Minh */
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SpLashViewModel::class)
    abstract fun spLashViewModel(viewModel: SpLashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BottomNavViewModel::class)
    abstract fun bottomNavViewModel(viewModel: BottomNavViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun homeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PhotoDetailViewModel::class)
    abstract fun photoDetailViewModel(viewModel: PhotoDetailViewModel): ViewModel


}

