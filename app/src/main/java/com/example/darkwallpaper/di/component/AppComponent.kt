package com.example.darkwallpaper.di.component

import com.example.darkwallpaper.di.DarkWallpaper
import com.example.darkwallpaper.di.module.ActivityModule
import com.example.darkwallpaper.di.module.ApiModule
import com.example.darkwallpaper.di.module.AppModule
import com.example.darkwallpaper.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Created by Duc Minh
 */

@Component(
    modules = [
        (AndroidInjectionModule::class),
        (ApiModule::class),
        (ActivityModule::class),
        (ViewModelModule::class),
        (AppModule::class)
    ]
)

@Singleton
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: DarkWallpaper): Builder

        fun build(): AppComponent
    }

    /*
     * This is our custom Application class
     * */
    fun inject(darkWallpaper: DarkWallpaper)
}