package com.example.darkwallpaper.di.module

import android.content.Context
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.darkwallpaper.DownloadFile
import com.example.darkwallpaper.base.manager.SharedPreferencesManager
import com.example.darkwallpaper.di.DarkWallpaper
import com.example.darkwallpaper.di.rx.SchedulerProvider
import com.example.darkwallpaper.model.remote.ApiRemote
import com.example.darkwallpaper.model.repository.ImagesRepository
import com.example.darkwallpaper.model.repository.ImagesRepositoryImp
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Named
import javax.inject.Singleton

/** Custom by Duc Minh */
@Module
class AppModule {

    @Singleton
    @Provides
    fun provideScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Provides
    @Singleton
    fun provideSchedulerProvider() = SchedulerProvider()

    @Provides
    @Singleton
    fun provideContext(app: DarkWallpaper): Context = app

    @Singleton
    @Provides
    fun provideSharedPreferencesManager(context: Context) = SharedPreferencesManager(context)

    @Provides
    @Named("staggered")
    fun provideStaggeredGridLayoutManager(context: Context) =
            StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

    @Singleton
    @Provides
    fun provideImagesRepository(apiRemote: ApiRemote)
            : ImagesRepository = ImagesRepositoryImp(apiRemote)

    @Singleton
    @Provides
    fun provideDownloadFile() = DownloadFile()

}