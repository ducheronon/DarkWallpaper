package com.example.darkwallpaper.di.module

import com.example.darkwallpaper.ui.bottomnav.BottomNavActivity
import com.example.darkwallpaper.ui.bottomnav.BottomNavProvider
import com.example.darkwallpaper.ui.photodetail.PhotoDetailActivity
import com.example.darkwallpaper.ui.splash.SpLashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Duc Minh
 */

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeSpLashActivity(): SpLashActivity

    @ContributesAndroidInjector(modules = [BottomNavProvider::class])
    abstract fun contributeBottomNavActivity(): BottomNavActivity

    @ContributesAndroidInjector
    abstract fun contributePhotoDetailActivity(): PhotoDetailActivity

}