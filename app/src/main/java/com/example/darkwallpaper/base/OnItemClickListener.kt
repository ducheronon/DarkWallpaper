package com.example.darkwallpaper.base

/** Created by Duc Minh */
interface OnItemClickListener {
    fun onItemClick(position: Int)
}