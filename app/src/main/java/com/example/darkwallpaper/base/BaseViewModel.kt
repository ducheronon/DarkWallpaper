package com.example.darkwallpaper.base

import androidx.lifecycle.ViewModel
import com.example.darkwallpaper.base.manager.SharedPreferencesManager
import com.example.darkwallpaper.di.rx.SchedulerProvider
import com.example.darkwallpaper.model.repository.ImagesRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/** Custom by Duc Minh */
abstract class BaseViewModel() : ViewModel() {

    companion object {
        const val SHOW_ERROR = 1
    }

    @Inject
    lateinit var schedulerProvider: SchedulerProvider

    @Inject
    lateinit var sharedPreferencesManager: SharedPreferencesManager

    @Inject
    lateinit var imagesRepository: ImagesRepository

    var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val mIsLoading = SingleLiveData<Boolean>()
    val uiEventLiveData = SingleLiveData<Int>()
    var errorMessage: Any = "Unknown error"

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

    fun launch(job: () -> Disposable) {
        compositeDisposable.add(job())
    }

    fun isLoading(): Boolean {
        return mIsLoading.value ?: false
    }

    fun getLoading(): SingleLiveData<Boolean>? {
        return mIsLoading
    }

    fun setLoading(isLoading: Boolean) {
        mIsLoading.value = isLoading
    }

    fun showError(message: Any) {
        errorMessage = message
        uiEventLiveData.value = SHOW_ERROR
    }

    open fun onStart() {}
    open fun onStop() {}
    open fun onResume() {}
}