package com.example.darkwallpaper.base.customview

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

/**
 * Copy by Duc Minh
 */

class ListItemDecoration(private val spacing: Int, private val checkPosition: Boolean) : ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view) // item position
        if (checkPosition) {
            if (position == parent.adapter?.itemCount!! - 1) {
                outRect.bottom = spacing
            }
        } else {
            outRect.top = spacing // item top
            outRect.left = spacing // item left
            outRect.right = spacing // item right
            outRect.bottom = spacing // item bottom
        }
    }
}