package com.example.darkwallpaper.ui.home

import androidx.lifecycle.MutableLiveData
import com.example.darkwallpaper.base.BaseViewModel
import com.example.darkwallpaper.model.images.ImagesItem
import javax.inject.Inject

class HomeViewModel @Inject constructor() : BaseViewModel() {

    private val mubImages = MutableLiveData<List<ImagesItem>>()
    private val mubInfoImageItem = MutableLiveData<ImagesItem>()

    fun liveDataImages(): MutableLiveData<List<ImagesItem>> = mubImages

    fun getListImages(page: Int, clientId: String) {
        compositeDisposable.add(
            imagesRepository.getImages(page, clientId)
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe(this::getImagesSuccess, this::getImagesFailed)
        )
    }

    private fun getImagesSuccess(imagesList: List<ImagesItem>) {
        mubImages.value = imagesList
    }

    private fun getImagesFailed(t: Throwable) {
        mubImages.value = null
    }

    fun getPositionOnClickImage(): MutableLiveData<ImagesItem> = mubInfoImageItem
    fun onItemClickGetPosition(position: Int) {
        mubImages.value?.let {
            mubInfoImageItem.value = it[position]
        }
    }
}