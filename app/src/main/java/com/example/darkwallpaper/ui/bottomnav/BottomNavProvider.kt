package com.example.darkwallpaper.ui.bottomnav

import com.example.darkwallpaper.FavoriteFragment
import com.example.darkwallpaper.MoreFragment
import com.example.darkwallpaper.MyWallpaperFragment
import com.example.darkwallpaper.ui.home.HomeFragment
import com.example.darkwallpaper.ui.home.module.HomeModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BottomNavProvider{

    @ContributesAndroidInjector(modules = [HomeModule::class])
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeFavoriteFragment(): FavoriteFragment

    @ContributesAndroidInjector
    abstract fun contributeMyWallpaperFragment(): MyWallpaperFragment

    @ContributesAndroidInjector
    abstract fun contributeMoreFragment(): MoreFragment

}