package com.example.darkwallpaper.ui.photodetail

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.darkwallpaper.BR
import com.example.darkwallpaper.DownloadFile
import com.example.darkwallpaper.R
import com.example.darkwallpaper.base.BaseActivity
import com.example.darkwallpaper.base.constants.Constants.PUT_OBJECT
import com.example.darkwallpaper.databinding.ActivityPhotoDetailBinding
import com.example.darkwallpaper.di.ViewModelFactory
import com.example.darkwallpaper.model.images.ImagesItem
import com.example.darkwallpaper.utils.NetworkUtils
import javax.inject.Inject


class PhotoDetailActivity : BaseActivity<ActivityPhotoDetailBinding, PhotoDetailViewModel>() {

    @Inject
    lateinit var factory: ViewModelFactory

    @Inject
    lateinit var downLoadFile: DownloadFile

    private lateinit var photoViewDetailViewModel: PhotoDetailViewModel

    override fun getBindingVariable(): Int = BR.photoDetailViewModel

    override fun getLayoutId(): Int = R.layout.activity_photo_detail

    override fun getViewModel(): PhotoDetailViewModel {
        photoViewDetailViewModel =
                ViewModelProvider(this, factory).get(PhotoDetailViewModel::class.java)
        return photoViewDetailViewModel
    }

    override fun updateUI(savedInstanceState: Bundle?) {
        val imagesItem: ImagesItem = intent.getSerializableExtra(PUT_OBJECT) as ImagesItem
        binding.imageItem = imagesItem
        binding.executePendingBindings()

        binding.btnDownload.setOnClickListener {
            if(NetworkUtils.isNetworkAvailable(this)) {
                downLoadFile.downloadImage(imagesItem.urls.full, this.applicationContext)
            }
        }
    }
}
