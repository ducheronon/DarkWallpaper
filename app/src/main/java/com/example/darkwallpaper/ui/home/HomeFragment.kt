package com.example.darkwallpaper.ui.home

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.darkwallpaper.BR
import com.example.darkwallpaper.R
import com.example.darkwallpaper.base.BaseFragment
import com.example.darkwallpaper.base.OnItemClickListener
import com.example.darkwallpaper.base.constants.Constants
import com.example.darkwallpaper.base.customview.ListItemDecoration
import com.example.darkwallpaper.databinding.FragmentHomeBinding
import com.example.darkwallpaper.di.ViewModelFactory
import com.example.darkwallpaper.ui.photodetail.PhotoDetailActivity
import javax.inject.Inject
import javax.inject.Named


class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(), OnItemClickListener {

    @Inject
    lateinit var factory: ViewModelFactory

    private lateinit var homeViewModel: HomeViewModel

    @Inject
    lateinit var homeAdapter: HomeAdapter

    @Inject
    @field:Named("staggered")
    lateinit var staggeredLayoutManager: StaggeredGridLayoutManager

    override fun getBindingVariable(): Int = BR.homeViewModel

    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun getViewModel(): HomeViewModel {
        homeViewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        return homeViewModel
    }

    override fun updateUI(savedInstanceState: Bundle?) {
        homeAdapter.setOnClickListener(this)

        staggeredLayoutManager.gapStrategy =
            StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS

        binding?.rclTopListImages?.apply {
            adapter = homeAdapter
            layoutManager = staggeredLayoutManager
            setHasFixedSize(true)
            addItemDecoration(
                ListItemDecoration(
                    resources.getDimensionPixelOffset(R.dimen._48sdp),
                    true
                )
            )
        }

        homeViewModel.getListImages(1, Constants.ACCESS_KEY)
        homeViewModel.liveDataImages().observe(viewLifecycleOwner, Observer {
            homeAdapter.clearItems()
            homeAdapter.addItems(it)
        })

        homeViewModel.getPositionOnClickImage().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                goScreenAndPutString(
                    requireActivity(),
                    PhotoDetailActivity::class.java,
                    it,
                    false,
                    R.anim.slide_in_right,
                    R.anim.slide_out_left
                )
            }
        })
    }

    override fun onItemClick(position: Int) {
        homeViewModel.onItemClickGetPosition(position)
    }
}