package com.example.darkwallpaper.ui.bottomnav

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.example.darkwallpaper.BR
import com.example.darkwallpaper.R
import com.example.darkwallpaper.base.BaseActivity
import com.example.darkwallpaper.databinding.ActivityBottomNavBinding
import com.example.darkwallpaper.di.ViewModelFactory
import javax.inject.Inject

class BottomNavActivity : BaseActivity<ActivityBottomNavBinding, BottomNavViewModel>() {

    @Inject
    lateinit var factory: ViewModelFactory

    private lateinit var bottomNavViewModel: BottomNavViewModel

    private lateinit var navController : NavController

    override fun getViewModel(): BottomNavViewModel {
       bottomNavViewModel = ViewModelProvider(this,factory).get(BottomNavViewModel::class.java)
        return bottomNavViewModel
    }

    override fun getLayoutId(): Int = R.layout.activity_bottom_nav

    override fun getBindingVariable(): Int = BR.bottomNavViewModel

    override fun updateUI(savedInstanceState: Bundle?) {
        navController = Navigation.findNavController(this, R.id.fragment)
        NavigationUI.setupWithNavController(binding.bottomNavigationView, navController)
        binding.bottomNavigationView.background = null
    }
}
