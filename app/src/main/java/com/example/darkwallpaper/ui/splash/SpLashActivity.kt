package com.example.darkwallpaper.ui.splash

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.darkwallpaper.BR
import com.example.darkwallpaper.R
import com.example.darkwallpaper.base.BaseActivity
import com.example.darkwallpaper.databinding.ActivitySpLashBinding
import com.example.darkwallpaper.di.ViewModelFactory
import com.example.darkwallpaper.ui.bottomnav.BottomNavActivity
import javax.inject.Inject

class SpLashActivity : BaseActivity<ActivitySpLashBinding, SpLashViewModel>() {

    @Inject
    lateinit var factory: ViewModelFactory

    private lateinit var spLashViewModel: SpLashViewModel

    override fun getViewModel(): SpLashViewModel {
        spLashViewModel = ViewModelProvider(this, factory).get(SpLashViewModel::class.java)
        return spLashViewModel
    }

    override fun getLayoutId(): Int = R.layout.activity_sp_lash

    override fun getBindingVariable(): Int = BR.spLashViewModel

    override fun updateUI(savedInstanceState: Bundle?) {
        binding.btnGetStarted.setOnClickListener {
            goScreen(BottomNavActivity::class.java,
                false,
                R.anim.slide_in_right,
                R.anim.slide_out_left
        ) }
    }
}