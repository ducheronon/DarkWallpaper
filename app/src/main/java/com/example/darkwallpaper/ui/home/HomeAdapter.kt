package com.example.darkwallpaper.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.darkwallpaper.R
import com.example.darkwallpaper.base.BaseAdapter
import com.example.darkwallpaper.base.BaseViewHolder
import com.example.darkwallpaper.base.OnItemClickListener
import com.example.darkwallpaper.databinding.AdapterItemImageBinding
import com.example.darkwallpaper.model.images.ImagesItem
import java.util.*

class HomeAdapter : BaseAdapter<ImagesItem, BaseViewHolder>() {

    private val imageItemList = ArrayList<ImagesItem>()
    private var onClickListener: OnItemClickListener? = null

    override fun addItems(repoList: List<ImagesItem>) {
        imageItemList.addAll(repoList)
        notifyDataSetChanged()
    }

    override fun clearItems() {
        imageItemList.clear()
    }

    fun setOnClickListener(listener: OnItemClickListener) {
        onClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return HomeViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.adapter_item_image,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = imageItemList.size

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    inner class HomeViewHolder(private val binding: AdapterItemImageBinding) :
        BaseViewHolder(binding.root) {
        override fun onBind(position: Int) {
            binding.imagesItem = imageItemList[position]
            binding.executePendingBindings()

            itemView.setOnClickListener {
                onClickListener?.onItemClick(position)
            }
        }
    }
}