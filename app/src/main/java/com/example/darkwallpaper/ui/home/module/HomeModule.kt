package com.example.darkwallpaper.ui.home.module

import com.example.darkwallpaper.ui.home.HomeAdapter
import dagger.Module
import dagger.Provides


@Module
class HomeModule {

    @Provides
    fun provideHomeAdapter() = HomeAdapter()
}