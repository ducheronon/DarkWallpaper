package com.example.darkwallpaper

import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.graphics.Insets
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.DisplayMetrics
import android.view.WindowInsets
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DownloadFile {

    private var loading = false
    private var downloadId: Long? = null
    private var downloadManagerQuery: DownloadManager.Query? = null

    fun downloadImage(url: String?, context: Context) {
        val request = DownloadManager.Request(Uri.parse(url))
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
        request.setTitle("Download")
        request.setDescription("The file is downloading...")
        request.setMimeType("image/jpg")
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                "${System.currentTimeMillis()}")


        //get download service and enqueue file
        val manager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        downloadId = manager.enqueue(request)

        GlobalScope.launch {
            loading = true
            try {
                downloadManagerQuery!!.setFilterById(downloadId!!)
                val cursor = manager.query(downloadManagerQuery)
                cursor.moveToFirst()

                val bytesDownloaded =cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR))
                val bytesTotal =cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES))

            } catch (e: Exception) {

            }
        }
    }

    private fun getScreenWidth(activity: Activity): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val windowMetrics = activity.windowManager.currentWindowMetrics
            val insets: Insets = windowMetrics.windowInsets
                    .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
            windowMetrics.bounds.width() - insets.left - insets.right;

        } else {
            val displayMetrics = DisplayMetrics()
            activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
            displayMetrics.widthPixels
        }
    }
}