package com.example.darkwallpaper.model.repository

import com.example.darkwallpaper.model.images.Images
import com.example.darkwallpaper.model.images.ImagesItem
import com.example.darkwallpaper.model.remote.ApiRemote
import io.reactivex.Observable

class ImagesRepositoryImp(private val apiRemote: ApiRemote) : ImagesRepository {

    override fun getImages(page: Int, clientId: String): Observable<List<ImagesItem>> {
        return apiRemote.getImages(page,clientId)
    }
}