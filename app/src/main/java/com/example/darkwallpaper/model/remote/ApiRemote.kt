package com.example.darkwallpaper.model.remote

import com.example.darkwallpaper.model.images.ImagesItem
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url


interface ApiRemote {

    @GET("photos")
    fun getImages(@Query("page") page:Int, @Query("client_id") clientId : String)
     : Observable<List<ImagesItem>>

    @GET
    fun downloadFileWithDynamicUrlSync(@Url fileUrl: String): Observable<ResponseBody>
}