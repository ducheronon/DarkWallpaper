package com.example.darkwallpaper.model.images

import java.io.Serializable

data class ProfileImageX(
    val large: String,
    val medium: String,
    val small: String
): Serializable