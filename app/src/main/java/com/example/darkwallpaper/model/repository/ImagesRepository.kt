package com.example.darkwallpaper.model.repository

import com.example.darkwallpaper.model.images.ImagesItem
import io.reactivex.Observable

interface ImagesRepository {

    fun getImages(page:Int, clientId : String) : Observable<List<ImagesItem>>
}