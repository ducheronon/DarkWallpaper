package com.example.darkwallpaper.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.darkwallpaper.R

/**
 * Created by Duc Minh
 */

class DataBindingUtils {

    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(imageView: ImageView, url: String?) {
            if (url.isNullOrEmpty()) {
                imageView.setImageResource(R.drawable.ic_image_place_holder)
            }
            else{ Glide.with(imageView.context).load(url).override(600,800).into(imageView) }
        }
    }
}