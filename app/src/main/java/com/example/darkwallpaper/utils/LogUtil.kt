package com.example.darkwallpaper.utils

import android.util.Log.*

/**
 * Copy by Duc Minh
 */

object LogUtil {

    var isDebug = false
    var tag = "Pro Đức Log"

    fun init(isDebug: Boolean) {
        LogUtil.isDebug = isDebug
    }

    fun verbose(tag: String, msg: String) {
        if (isDebug) {
            v(tag, msg)
        }
    }

    fun verbose(msg: String) {
        if (isDebug) {
            v(tag, msg)
        }
    }

    fun debug(tag: String, msg: String) {
        if (isDebug) {
            d(tag, msg)
        }
    }

    fun debug(msg: String) {
        if (isDebug) {
            d(tag, msg)
        }
    }

    fun info(tag: String, msg: String) {
        if (isDebug) {
            i(tag, msg)
        }
    }

    fun info(msg: String) {
        if (isDebug) {
            i(tag, msg)
        }
    }

    fun warning(tag: String, msg: String) {
        if (isDebug) {
            w(tag, msg)
        }
    }

    fun warning(msg: String) {
        if (isDebug) {
            w(tag, msg)
        }
    }

    fun error(tag: String, msg: String) {
        if (isDebug) {
            e(tag, msg)
        }
    }

    fun error(msg: String) {
        if (isDebug) {
            e(tag, msg)
        }
    }
}
